FROM php:7.4-fpm

WORKDIR /app

# Install dependencies for the operating system software
RUN apt-get update -y && apt-get install -y \
build-essential \
libpng-dev \
libjpeg62-turbo-dev \
libfreetype6-dev \
locales \
zip \
jpegoptim optipng pngquant gifsicle \
vim \
libzip-dev \
unzip \
git \
libonig-dev \
curl

RUN docker-php-ext-install gd mbstring
RUN docker-php-ext-configure gd --with-freetype --with-jpeg

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install composer (php package manager)
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY . /app

RUN composer install
RUN cp .env.example .env
RUN php artisan key:generate
CMD php artisan serve --host=0.0.0.0 --port=8000
EXPOSE 8000
