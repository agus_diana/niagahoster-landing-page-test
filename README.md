# Niagahoster Landing Page Test

[Preview The Project](http://35.219.27.3:8081) *(online between 08:00 AM and 05:00 PM)*

## Tech Stack
- HTML
- Laravel 8
- Bootstrap 4
- jQuery 3
- php 7.3 (minimum requirement)
- Docker

## Installation
- `git clone https://gitlab.com/agus_diana/niagahoster-landing-page-test.git`
- `cd niagahoster-landing-page-test`

## Running Application With Docker
- `docker build -t agus/landing_page .`
- `docker run -p 8000:8000 agus/landing_page`
- `open http://localhost:8000 on your favorite web browser`

## Running Application Without Docker
- `copy .env.example .env`
- `composer install`
- `php artisan key:generate`
- `php artisan serve`
- `open http://localhost:8000 on your favorite web browser`

## License
The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
