<?php
/**
 * Created by IntelliJ IDEA.
 * User: agus
 * Date: 24/12/2021
 * Time: 06.59
 */

namespace App\Http\Controllers;

class LandingPageController extends Controller
{
    public function index(){
        $path = base_path('data.json');
        $json_data = json_decode(file_get_contents($path), true);

        return view('landingpage', [
            "packages" => $json_data['packages'],
            "navigations" => $json_data['navigations'],
            "available_modules" => $json_data['available_modules']
        ]);
    }
}
