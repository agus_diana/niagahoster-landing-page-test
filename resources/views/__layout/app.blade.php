<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">

    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</head>
<body>
    {{-- Header --}}
    <div class="d-none d-md-flex container flex-row justify-content-between align-items-center top-banner">
        <div class="d-flex align-items-center">
            <img src="{{ asset('assets/images/pricetag.png') }}" width="28" height="auto" class="d-inline-block align-top mr-2" alt="">
            <small>Gratis Ebook 9 Cara Cerdas Menggunakan Domain [x]</small>
        </div>
        <div class="d-flex flex-row justify-content-end">
            <a href="#" class="mx-3"><i class="fas fa-phone-alt"></i> 0123-123123</a>
            <a href="#" class="mx-3"><i class="fas fa-comments"></i> Live Chat</a>
            <a href="#" class="mx-3"><i class="fas fa-user-circle"></i> Member Area</a>
        </div>
    </div>
    <nav class="navbar navbar-light bg-white navbar-expand-sm border-top border-bottom">
        <div class="container">
            <a class="navbar-brand" href="#">
                <img src="{{ asset('assets/images/logo.png') }}" width="auto" height="64" class="d-inline-block align-top" alt="">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-list-2" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbar-list-2">
                <ul class="navbar-nav ml-auto">
                    @foreach ($navigations as $nav)
                        <li class="nav-item active">
                            <a class="nav-link" href="#">{{$nav}}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    {{-- Footer --}}
    <div class="footer-social d-flex flex-row align-items-center py-2">
        <div class="container">
            <div class="row d-flex flex-row align-items-center justify-content-between">
                <div class="col-12 col-md-6 col-sm-12">
                    <span>Bagikan jika anda menyukai halaman ini</span>
                </div>
                <div class="col-12 col-md-6 col-sm-12 border-white border-lg-left px-md-4 text-left">
                    <img src="{{ asset('assets/images/fb1.png') }}" height="100%" width="auto" class="mr-1">
                    <img src="{{ asset('assets/images/twt1.png') }}" height="100%" width="auto" class="mr-1">
                    <img src="{{ asset('assets/images/g1.png') }}" height="100%" width="auto" class="mr-1">
                </div>
            </div>
        </div>
    </div>
    <div class="footer-contact d-flex flex-row align-items-center">
        <div class="container">
            <div class="row d-flex flex-row align-items-center justify-content-between">
                <div class="col-12 col-lg-8 col-sm-12">
                    <span>Perlu <b>BANTUAN?</b> Hubungi Kami: <b>0274-5305505</b></span>
                </div>
                <div class="col-12 col-lg-4 col-sm-12 border-white border-lg-left px-md-4 text-left text-md-right">
                    <button class="btn btn-outline-light rounded-pill py-2 px-4 font-bold"><i class="fas fa-comments"></i> <b>Live Chat</b></button>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-main">
        <div class="container">
            <div class="row">
                <div class="col-6 col-md-3 d-flex flex-column mb-3">
                    <strong>HUBUNGI KAMI</strong>
                    <ul>
                        <li>0274-5305505</li>
                        <li>Senin - Minggu</li>
                        <li>24 Jam Nonstop</li>
                    </ul>
                    <ul>
                        <li>Jl. Selokan Mataram Monjali</li>
                        <li>Karangjati MT I/304</li>
                        <li>Sinduadi, Mlati, Sleman</li>
                        <li>Yohyakarta 55284</li>
                    </ul>
                </div>
                <div class="col-6 col-md-3 d-flex flex-column mb-3">
                    <strong>LAYANAN</strong>
                    <ul>
                        <li>Domain</li>
                        <li>Shared Hosting</li>
                        <li>Cloud VPS Hosting</li>
                        <li>Managed VPS Hosting</li>
                        <li>Web Builder</li>
                        <li>Keamanan SSL /HTTPS</li>
                        <li>Jasa Pembuatan Website</li>
                        <li>Program Affiliasi</li>
                    </ul>
                </div>
                <div class="col-6 col-md-3 d-flex flex-column mb-3">
                    <strong>SERVICE HOSTING</strong>
                    <ul>
                        <li>Hosting Mudah</li>
                        <li>Hosting Indoneisa</li>
                        <li>Hosting Singapura SG</li>
                        <li>Hosting PHP</li>
                        <li>Hosting Wordpress</li>
                        <li>Hosting Laravel</li>
                    </ul>
                </div>
                <div class="col-6 col-md-3 d-flex flex-column mb-3">
                    <strong>TUTORIAL</strong>
                    <ul>
                        <li>Knowledgebase</li>
                        <li>Blog</li>
                        <li>Cara Pembayaran</li>
                    </ul>
                </div>
                <div class="col-6 col-md-3 d-flex flex-column mb-3">
                    <strong>TENTANG KAMI</strong>
                    <ul>
                        <li>Tim Niagahoster</li>
                        <li>Karir</li>
                        <li>Events</li>
                        <li>Penawaran & Promo Spesial</li>
                        <li>Kontak Kami</li>
                    </ul>
                </div>
                <div class="col-6 col-md-3 d-flex flex-column mb-3">
                    <strong>KENAPA PILIH NIAGAHOSTER?</strong>
                    <ul>
                        <li>Tim Niagahoster</li>
                        <li>Karir</li>
                        <li>Events</li>
                        <li>Penawaran & Promo Spesial</li>
                        <li>Kontak Kami</li>
                    </ul>
                </div>
                <div class="col-12 col-md-3 d-flex flex-column mb-3">
                    <strong>NEWSLETTER</strong>
                    <div class="position-relative newsletter-form mb-2">
                        <input type="text" class="form-control rounded-pill p-4" placeholder="Email">
                        <button type="button" class="btn btn-primary rounded-pill">Berlangganan</button>
                    </div>
                    <small>Dapatkan promo dan konten menarik dari penyedia hosting terbaik Anda.</small>
                </div>
                <div class="col-8 col-md-3 d-flex flex-row align-items-start justify-content-between mb-3">
                    <img src="{{ asset('assets/images/fb.png') }}" width="48" height="auto" alt="">
                    <img src="{{ asset('assets/images/twt.png') }}" width="48" height="auto" alt="">
                    <img src="{{ asset('assets/images/g.png') }}" width="48" height="auto" alt="">
                </div>
                <div class="col col-12 d-flex flex-column mb-3">
                    <strong>PEMBAYARAN</strong>
                    <div class="row mb-3">
                        <div class="col col-lg-1 col-sm-3 col-md-4 m-1">
                            <img src="{{ asset('assets/images/bca.png') }}" height="48" width="auto" alt="">
                        </div>
                        <div class="col col-lg-1 col-sm-3 col-md-4 m-1">
                            <img src="{{ asset('assets/images/mandiri.png') }}" height="48" width="auto" alt="">
                        </div>
                        <div class="col col-lg-1 col-sm-3 col-md-4 m-1">
                            <img src="{{ asset('assets/images/bni.png') }}" height="48" width="auto" alt="">
                        </div>
                        <div class="col col-lg-1 col-sm-3 col-md-4 m-1">
                            <img src="{{ asset('assets/images/visa.png') }}" height="48" width="auto" alt="">
                        </div>
                        <div class="col col-lg-1 col-sm-3 col-md-4 m-1">
                            <img src="{{ asset('assets/images/mastercard.png') }}" height="48" width="auto" alt="">
                        </div>
                        <div class="col col-lg-1 col-sm-3 col-md-4 m-1">
                            <img src="{{ asset('assets/images/atm.png') }}" height="48" width="auto" alt="">
                        </div>
                        <div class="col col-lg-1 col-sm-3 col-md-4 m-1">
                            <img src="{{ asset('assets/images/permata.png') }}" height="48" width="auto" alt="">
                        </div>
                        <div class="col col-lg-1 col-sm-3 col-md-4 m-1">
                            <img src="{{ asset('assets/images/prima.png') }}" height="48" width="auto" alt="">
                        </div>
                        <div class="col col-lg-1 col-sm-3 col-md-4 m-1">
                            <img src="{{ asset('assets/images/alto.png') }}" height="48" width="auto" alt="">
                        </div>
                    </div>
                    <small>Aktivasi instan dengan e-Payment Hosting dan domain langsung aktif!</small>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-8">
                    <p style="font-size: 12px; color: #e2e2e2">Copyright &copy;{{date('Y')}} Niagahoster | Hosting powered by PHP7, CloudLinux, CloudFlare, BitNinja and DC Biznet Technovillage Jakarta cloud VPS Murah powered by Webuzo softaculous, Intel SSD and cloud computing technology</p>
                </div>
                <div class="col-4">
                    <p style="font-size: 12px; color: #e2e2e2" class="float-right">Syarat dan Ketentuan | Kebijakan Privasi</p>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
