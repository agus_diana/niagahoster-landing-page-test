@extends('__layout.app')

@section('content')
    <main class="p-0">

        <div class="mb-4 border-bottom text-center">
            <div class="container d-flex flex-row align-items-center headline">
                <div class="row w-100">
                    <div class="col-md-6 d-flex flex-column py-4 text-left">
                        <h1 class="text1">PHP Hosting</h1>
                        <h2 class="text2">Cepat, handal, penuh dengan modul PHP yang Anda butuhkan</h2>
                        <div class="mt-4">
                            <p><i class="fas fa-check-circle text-success"></i> Solusi PHP untuk performa query yang lebih cepat.</p>
                            <p><i class="fas fa-check-circle text-success"></i> Konsumsi memori yang lebih rendah.</p>
                            <p><i class="fas fa-check-circle text-success"></i> Support PHP 5.3, PHP 5.4, PHP 5.5, PHP 5.6, PHP 7</p>
                            <p><i class="fas fa-check-circle text-success"></i> Fitur enkripsi IonCube dan Zend Guard Loaders</p>
                        </div>
                        @if (isset($headline['button']))
                            <button class="btn btn-primary rounded-pill btn-main">{{$headline['button']}}</button>
                        @endif
                    </div>
                    <div class="d-none d-md-flex col-6 flex-row align-items-center">
                        <img src="{{ asset('assets/svg/illustration banner PHP hosting-01.svg') }}" class="img-fluid" height="100%"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="mb-4 d-flex justify-content-center">
            <div class="container">
                <div class="row">
                    <div class="col-4 d-flex flex-column text-center">
                        <div class="tech-img d-flex justify-content-center align-items-center">
                            <img src="{{ asset('assets/images/zenguard.png') }}" class="img-fluid" height="auto"/>
                        </div>
                        <span>PHP Zend Guard Loader</span>
                    </div>
                    <div class="col-4 d-flex flex-column text-center">
                        <div class="tech-img d-flex justify-content-center align-items-center">
                            <img src="{{ asset('assets/images/composer.png') }}" class="img-fluid" height="auto"/>
                        </div>
                        <span>PHP Composer</span>
                    </div>
                    <div class="col-4 d-flex flex-column text-center">
                        <div class="tech-img d-flex justify-content-center align-items-center">
                            <img src="{{ asset('assets/images/ioncube.png') }}" class="img-fluid" height="auto"/>
                        </div>
                        <span>PHP ionCube Loader</span>
                    </div>
                </div>
            </div>
        </div>

        <div class="my-5 d-flex flex-column align-items-center text-center paket">
            <div class="container">
                <h1 class="title">Paket Hosting Singapura yang Tepat</h1>
                <h3 class="subtitle">Diskon 40% Domain dan SSL Gratis untuk Anda</h3>
                <div class="row d-flex justify-content-center align-items-start">
                    @foreach ($packages as $i)
                        <div class="col-md-3 paket mb-2 p-md-0 position-relative">
                            @if ($i['is_best'])
                                <div class="ribbon ribbon-top-left"><span>Best Seller!</span></div>
                            @endif
                            <ul class="list-group rounded {{$i['is_best'] ? 'border border-primary' : ''}}">
                                <li class="list-group-item name {{$i['is_best'] ? 'bg-primary text-white' : ''}}">{{$i['name']}}</li>
                                <li class="list-group-item price {{$i['is_best'] ? 'bg-primary text-white' : ''}}">
                                    <div class="price2">Rp {{number_format($i['price2'])}}</div>
                                    <?php
                                    $price_in_array = explode(',',number_format($i['price1']));
                                    ?>
                                    <div class="price1 d-flex flex-row align-items-start justify-content-center">
                                        <span class="mr-2">Rp</span>
                                        <span class="big">{{$price_in_array[0]}}</span>
                                        <span>.{{number_format($price_in_array[1])}}</span>
                                    </div>
                                </li>
                                <li class="list-group-item {{$i['is_best'] ? 'bg-primary text-white' : ''}}"><strong>{{number_format($i['reg_count'])}}</strong> Pengguna terdaftar</li>
                                <li class="list-group-item">
                                    {!! $i['feature'] !!}
                                    <div class="my-4">
                                        <button class="btn btn-outline-secondary rounded-pill {{$i['is_best'] ? 'best' : ''}}">{{$i['button']}}</button>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="mb-4 d-flex flex-column text-center">
            <div class="container">
                <h3 class="subtitle">Powerful dengan Limit PHP yang Lebih Besar</h3>
                <div class="d-none row d-md-flex justify-content-center">
                    <div class="col-5">
                        <table class="table table-striped table-bordered table-sm">
                            <tbody>
                            <tr>
                                <td><i class="fas fa-check-circle text-success"></i></td>
                                <td>max execution time 300s</td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-check-circle text-success"></i></td>
                                <td>max execution time 300s</td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-check-circle text-success"></i></td>
                                <td>php memory limit 1024 MB</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-5">
                        <table class="table table-striped table-bordered table-sm">
                            <tbody>
                            <tr>
                                <td><i class="fas fa-check-circle text-success"></i></td>
                                <td>post max size 128 MB</td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-check-circle text-success"></i></td>
                                <td>upload max filesize 128 MB</td>
                            </tr>
                            <tr>
                                <td><i class="fas fa-check-circle text-success"></i></td>
                                <td>max input vars 2500</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="mb-4 d-flex flex-column text-center fitur">
            <div class="container">
                <h3 class="subtitle">Semua Paket Hosting Sudah Termasuk</h3>
                <div class="row d-flex">
                    <div class="col-6 col-md-4 d-flex flex-column p-5">
                        <img src="{{ asset('assets/svg/icon PHP Hosting_PHP Semua Versi.svg')}}" height="54" width="auto"/>
                        <strong>PHP Semua Versi</strong>
                        <p>Pilih mulai dari versi PHP 5.3 s/d PHP 7. Ubah sesuka Anda!</p>
                    </div>
                    <div class="col-6 col-md-4 d-flex flex-column p-5">
                        <img src="{{ asset('assets/svg/icon PHP Hosting_My SQL.svg')}}" height="54" width="auto"/>
                        <strong>MySQL Versi 5.6</strong>
                        <p>Nikmati MySQL versi terharu, tercepat dan kaya akan fitur.</p>
                    </div>
                    <div class="col-6 col-md-4 d-flex flex-column p-5">
                        <img src="{{ asset('assets/svg/icon PHP Hosting_CPanel.svg')}}" height="54" width="auto"/>
                        <strong>Panel Hosting cPanel</strong>
                        <p>Kelola website dengan panel canggih yang familiar di hall Anda.</p>
                    </div>
                    <div class="col-6 col-md-4 d-flex flex-column p-5">
                        <img src="{{ asset('assets/svg/icon PHP Hosting_garansi uptime.svg')}}" height="54" width="auto"/>
                        <strong>Garansi Uptime 99.9%</strong>
                        <p>Data center yang mendukung kelangsungan website Anda 24/7.</p>
                    </div>
                    <div class="col-6 col-md-4 d-flex flex-column p-5">
                        <img src="{{ asset('assets/svg/icon PHP Hosting_InnoDB.svg')}}" height="54" width="auto"/>
                        <strong>Database lnnoDB Unlimited</strong>
                        <p>Jumlah dan ukuran database yang tumbuh sesuai kebutuhan Anda.</p>
                    </div>
                    <div class="col-6 col-md-4 d-flex flex-column p-5">
                        <img src="{{ asset('assets/svg/icon PHP Hosting_My SQL remote.svg')}}" height="54" width="auto"/>
                        <strong>Wildcard Remote MySQL</strong>
                        <p>Mendukung s/d 25 max_user_connections dan 100 max_connections.</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="mb-4 border-bottom text-center">
            <h3 class="subtitle">Mendukung Penuh Framework Laravel</h3>
            <div class="container d-flex flex-row align-items-center headline">
                <div class="row w-100">
                    <div class="col-md-6 d-flex flex-column py-4 text-left">
                        <h2 class="text3">Tak perlu menggunakan dedicated server ataupun VPS yang mahal. Layanan PHP hosting murah kami mendukung penuh framework favorit Anda</h2>
                        <div class="mt-4 benefit">
                            <p class="text5"><i class="fas fa-check-circle text-success"></i> Install Laravel <b>1 klik</b> dengan Softaculous Installer.</p>
                            <p class="text5"><i class="fas fa-check-circle text-success"></i> Mendukung ekstensi <b>PHP MCrypt, phar, mbstring, json,</b> dan <b>fileinfo.</b></p>
                            <p class="text5"><i class="fas fa-check-circle text-success"></i> Tersedia <b>Composer</b> dan <b>SSH</b> untuk menginstall packages pilihan Anda.</p>
                        </div>
                        <div class="my-2 my-md-2">
                            <small style="font-family: 'Roboto', sans-serif" >Nb. Composer dan SSH hanya tersedia pada paket Personal dan Bisnis</small>
                        </div>
                        <div class="my-2 my-md-4">
                            <button class="btn btn-primary rounded-pill btn-main" style="font-family: 'Roboto', sans-serif; font-size: 20px">Pilih Hosting Anda</button>
                        </div>
                    </div>
                    <div class="d-none d-md-flex col-6 flex-row align-items-center">
                        <img src="{{ asset('assets/svg/illustration banner support laravel hosting.svg')}}" class="img-fluid" height="100%"/>
                    </div>
                </div>
            </div>
        </div>

        <div class="mb-4 d-flex flex-column text-center">
            <div class="container">
                <h3 class="subtitle">Modul Lengkap untuk Menjalankan Aplikasi PHP Anda.</h3>
                <div class="row d-flex my-2 my-md-4">
                    @foreach ($available_modules as $module)
                        <div class="col-6 col-lg-3 d-flex flex-column text-left"><span>{{ $module }}</span></div>
                    @endforeach
                </div>
                <button class="btn btn-outline-secondary rounded-pill btn-secondary">Selengkapnya</button>
            </div>
        </div>

        <div class="mb-4 border-bottom text-center">
            <div class="container d-flex flex-row align-items-center headline">
                <div class="row w-100">
                    <div class="col-md-6 d-flex flex-column py-4 text-left">
                        <h3 class="text2">Linux Hosting yang Stabil<br/>dengan Teknologi LVE</h3>
                        <p class="text4">SuperMicro <b>Intel Xeon 24-Cores</b> server dengan RAM <b>128 GB</b> dan teknologi <b>LIVE CloudLinux</b> untuk stabilitas server Anda. Dilengkapi dengan <b>SSD</b> untuk kecepatan <b>MySQL</b> dan caching, Apache load balancer berbasis LiteSpeed Technologies, <b>CageFS</b> security, <b>Raid-10</b> protection dan auto backup untuk keamanan website PHP Anda.</p>
                        <div class="my-2 my-md-4">
                            <button class="btn btn-primary rounded-pill btn-main">Pilih Hosting Anda</button>
                        </div>
                    </div>
                    <div class="d-none d-md-flex col-6 flex-row align-items-center">
                        <img src="{{ asset('assets/images/Image support.png') }}" class="img-fluid" height="100%"/>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
